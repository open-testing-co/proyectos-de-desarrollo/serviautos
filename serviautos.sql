-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: wordpress
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_commentmeta`
--

LOCK TABLES `wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `wp_commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_links`
--

LOCK TABLES `wp_links` WRITE;
/*!40000 ALTER TABLE `wp_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2560 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_options`
--

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;
INSERT INTO `wp_options` VALUES (1,'siteurl','http://serviautos.tk/','yes'),(2,'home','http://serviautos.tk/','yes'),(3,'blogname','Serviautos Fernandez','yes'),(4,'blogdescription','Su técnico amigo','yes'),(5,'users_can_register','1','yes'),(6,'admin_email','h.andresc1127@gmail.com','yes'),(7,'start_of_week','1','yes'),(8,'use_balanceTags','0','yes'),(9,'use_smilies','1','yes'),(10,'require_name_email','1','yes'),(11,'comments_notify','1','yes'),(12,'posts_per_rss','10','yes'),(13,'rss_use_excerpt','0','yes'),(14,'mailserver_url','mail.example.com','yes'),(15,'mailserver_login','login@example.com','yes'),(16,'mailserver_pass','password','yes'),(17,'mailserver_port','110','yes'),(18,'default_category','1','yes'),(19,'default_comment_status','open','yes'),(20,'default_ping_status','open','yes'),(21,'default_pingback_flag','1','yes'),(22,'posts_per_page','10','yes'),(23,'date_format','F j, Y','yes'),(24,'time_format','g:i a','yes'),(25,'links_updated_date_format','F j, Y g:i a','yes'),(26,'comment_moderation','0','yes'),(27,'moderation_notify','1','yes'),(28,'permalink_structure','/%postname%/','yes'),(29,'rewrite_rules','a:87:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}','yes'),(30,'hack_file','0','yes'),(31,'blog_charset','UTF-8','yes'),(32,'moderation_keys','','no'),(33,'active_plugins','a:4:{i:0;s:19:\"akismet/akismet.php\";i:1;s:29:\"easy-wp-smtp/easy-wp-smtp.php\";i:2;s:27:\"simple-gomaps-10/gomaps.php\";i:3;s:33:\"yandex-metrica/yandex-metrica.php\";}','yes'),(34,'category_base','','yes'),(35,'ping_sites','http://rpc.pingomatic.com/','yes'),(36,'comment_max_links','2','yes'),(37,'gmt_offset','','yes'),(38,'default_email_category','1','yes'),(39,'recently_edited','a:5:{i:0;s:67:\"/var/www/html/wordpress/wp-content/themes/twentyseventeen/style.css\";i:1;s:65:\"/var/www/html/wordpress/wp-content/themes/twentysixteen/style.css\";i:2;s:98:\"/var/www/html/wordpress/wp-content/themes/twentyseventeen/template-parts/footer/footer-widgets.php\";i:3;s:68:\"/var/www/html/wordpress/wp-content/themes/twentyseventeen/footer.php\";i:4;s:70:\"/var/www/html/wordpress/wp-content/themes/twentyseventeen/comments.php\";}','no'),(40,'template','twentyseventeen','yes'),(41,'stylesheet','twentyseventeen','yes'),(42,'comment_whitelist','1','yes'),(43,'blacklist_keys','','no'),(44,'comment_registration','0','yes'),(45,'html_type','text/html','yes'),(46,'use_trackback','0','yes'),(47,'default_role','subscriber','yes'),(48,'db_version','43764','yes'),(49,'uploads_use_yearmonth_folders','1','yes'),(50,'upload_path','','yes'),(51,'blog_public','1','yes'),(52,'default_link_category','2','yes'),(53,'show_on_front','posts','yes'),(54,'tag_base','','yes'),(55,'show_avatars','1','yes'),(56,'avatar_rating','G','yes'),(57,'upload_url_path','','yes'),(58,'thumbnail_size_w','150','yes'),(59,'thumbnail_size_h','150','yes'),(60,'thumbnail_crop','1','yes'),(61,'medium_size_w','300','yes'),(62,'medium_size_h','300','yes'),(63,'avatar_default','mystery','yes'),(64,'large_size_w','1024','yes'),(65,'large_size_h','1024','yes'),(66,'image_default_link_type','none','yes'),(67,'image_default_size','','yes'),(68,'image_default_align','','yes'),(69,'close_comments_for_old_posts','0','yes'),(70,'close_comments_days_old','14','yes'),(71,'thread_comments','1','yes'),(72,'thread_comments_depth','5','yes'),(73,'page_comments','0','yes'),(74,'comments_per_page','50','yes'),(75,'default_comments_page','newest','yes'),(76,'comment_order','asc','yes'),(77,'sticky_posts','a:0:{}','yes'),(78,'widget_categories','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(79,'widget_text','a:5:{i:1;a:0:{}i:2;a:4:{s:5:\"title\";s:13:\"Quienes somos\";s:4:\"text\";s:423:\"Somos una empresa dedicada al mantenimiento y reparación de vehículos, contamos con herramientas especializadas y amplia experiencia en el sector automotriz para brindarle el mejor servicio con garantía, seriedad, calidad y cumplimiento.\r\n\r\n<img class=\"size-medium wp-image-6 aligncenter\" src=\"http://18.191.134.65/wordpress/wp-content/uploads/2018/12/Serviautos-Fernandez-298x300.png\" alt=\"\" width=\"298\" height=\"300\" />\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:3;a:4:{s:5:\"title\";s:10:\"Visítanos\";s:4:\"text\";s:125:\"Horario:\r\n\r\nLunes - Sabado: 8am - 6pm\r\n\r\nCEL: ‪3222951180‬\r\n\r\nCl 10 #16b-45\r\n\r\nSector El hueco.\r\n\r\nEl Peñol, Antioquía\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:4;a:4:{s:5:\"title\";s:18:\"Nuestros Servicios\";s:4:\"text\";s:208:\"Prestamos los servicios de mantenimiento preventivo, reparación de suspensión, transmisión, frenos, embrague, motor, transmisión, soldadura y mecánica en general para vehículos toda clase de vehículos.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}s:12:\"_multiwidget\";i:1;}','yes'),(80,'widget_rss','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),(81,'uninstall_plugins','a:1:{s:29:\"easy-wp-smtp/easy-wp-smtp.php\";s:17:\"swpsmtp_uninstall\";}','no'),(82,'timezone_string','America/Bogota','yes'),(83,'page_for_posts','0','yes'),(84,'page_on_front','0','yes'),(85,'default_post_format','0','yes'),(86,'link_manager_enabled','0','yes'),(87,'finished_splitting_shared_terms','1','yes'),(88,'site_icon','0','yes'),(89,'medium_large_size_w','768','yes'),(90,'medium_large_size_h','0','yes'),(91,'wp_page_for_privacy_policy','3','yes'),(92,'show_comments_cookies_opt_in','0','yes'),(93,'initial_db_version','38590','yes'),(94,'wp_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes'),(95,'fresh_site','0','yes'),(96,'widget_search','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(97,'widget_recent-posts','a:2:{i:3;a:3:{s:5:\"title\";s:15:\"Lo mas reciente\";s:6:\"number\";i:5;s:9:\"show_date\";b:0;}s:12:\"_multiwidget\";i:1;}','yes'),(98,'widget_recent-comments','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(99,'widget_archives','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(100,'widget_meta','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(101,'sidebars_widgets','a:5:{s:19:\"wp_inactive_widgets\";a:2:{i:0;s:14:\"recent-posts-3\";i:1;s:13:\"media_video-2\";}s:9:\"sidebar-1\";a:2:{i:0;s:6:\"text-2\";i:1;s:6:\"text-4\";}s:9:\"sidebar-2\";a:1:{i:0;s:6:\"text-3\";}s:9:\"sidebar-3\";a:1:{i:0;s:13:\"custom_html-2\";}s:13:\"array_version\";i:3;}','yes'),(102,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(103,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(104,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(105,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(106,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(107,'widget_media_video','a:2:{i:2;a:11:{s:13:\"attachment_id\";i:0;s:3:\"url\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"preload\";s:8:\"metadata\";s:4:\"loop\";b:0;s:7:\"content\";s:0:\"\";s:3:\"mp4\";s:0:\"\";s:3:\"m4v\";s:0:\"\";s:4:\"webm\";s:0:\"\";s:3:\"ogv\";s:0:\"\";s:3:\"flv\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),(108,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(109,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(110,'widget_custom_html','a:2:{i:2;a:2:{s:5:\"title\";s:10:\"Ubicación\";s:7:\"content\";s:390:\"<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d991.588222123346!2d-75.24244367086703!3d6.217109532936303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e441de73465acd9%3A0xbdba2de0354f112a!2sCl.+8+%2315a-73+a+15a-1%2C+Pe%C3%B1ol%2C+Antioquia!5e0!3m2!1ses-419!2sco!4v1543934556259\" width=\"800\" height=\"300\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\";}s:12:\"_multiwidget\";i:1;}','yes'),(111,'cron','a:5:{i:1564516512;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1564523712;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1564523744;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564583712;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}','yes'),(112,'theme_mods_twentyseventeen','a:7:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1543932994;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}s:18:\"nav_menu_locations\";a:0:{}s:12:\"header_image\";s:122:\"http://18.191.134.65/wordpress/wp-content/uploads/2018/12/cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03.jpg\";s:12:\"header_video\";i:0;s:11:\"custom_logo\";i:26;s:17:\"header_image_data\";O:8:\"stdClass\":5:{s:13:\"attachment_id\";i:9;s:3:\"url\";s:122:\"http://18.191.134.65/wordpress/wp-content/uploads/2018/12/cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03.jpg\";s:13:\"thumbnail_url\";s:122:\"http://18.191.134.65/wordpress/wp-content/uploads/2018/12/cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03.jpg\";s:6:\"height\";i:1200;s:5:\"width\";i:2000;}}','yes'),(142,'recently_activated','a:0:{}','yes'),(153,'widget_akismet_widget','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(182,'WPLANG','es_CO','yes'),(183,'new_admin_email','h.andresc1127@gmail.com','yes'),(192,'current_theme','Twenty Seventeen','yes'),(193,'theme_mods_beauty-studio','a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1543930636;s:4:\"data\";a:9:{s:19:\"wp_inactive_widgets\";a:0:{}s:21:\"beauty-studio-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:18:\"beauty-studio-home\";a:0:{}s:26:\"beauty-studio-sidebar-left\";a:0:{}s:14:\"footer-col-one\";a:0:{}s:14:\"footer-col-two\";a:0:{}s:16:\"footer-col-three\";a:0:{}s:15:\"footer-col-four\";a:0:{}s:17:\"popup-widget-area\";a:0:{}}}}','yes'),(194,'theme_switched','','yes'),(195,'widget_beauty_studio_accordion','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(196,'widget_beauty_studio_posts_col','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(197,'widget_beauty_studio_contact','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(198,'widget_beauty_studio_service','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(199,'widget_beauty_studio_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(200,'widget_beauty_studio_social','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(201,'widget_beauty_studio_team','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(202,'widget_beauty_studio_testimonial','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(203,'widget_beauty_studio_feature','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(210,'category_children','a:0:{}','yes'),(213,'_transient_beauty_studio_categories','1','yes'),(220,'theme_mods_best-construction','a:7:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";i:7;s:16:\"header_textcolor\";s:6:\"dd1616\";s:16:\"background_color\";s:6:\"b7b7b7\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1543932975;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:19:\"sidebar-woocommerce\";a:0:{}}}}','yes'),(228,'theme_mods_twentyfifteen','a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1543933011;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes'),(229,'_transient_twentyfifteen_categories','1','yes'),(240,'api_gomaps','','yes'),(241,'type_gomaps','user,post','yes'),(312,'db_upgraded','','yes'),(314,'can_compress_scripts','0','no'),(396,'auto_core_update_notified','a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:23:\"h.andresc1127@gmail.com\";s:7:\"version\";s:5:\"5.0.4\";s:9:\"timestamp\";i:1552449638;}','no'),(1617,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:5:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.1\";s:7:\"version\";s:5:\"5.2.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.2-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.2\";s:7:\"version\";s:3:\"5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:4;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.1.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.1.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.1\";s:7:\"version\";s:5:\"5.1.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1564480832;s:15:\"version_checked\";s:5:\"5.0.4\";s:12:\"translations\";a:0:{}}','no'),(1763,'swpsmtp_options','a:8:{s:16:\"from_email_field\";s:32:\"automatizacionmicuenta@gmail.com\";s:15:\"from_name_field\";s:10:\"Serviautos\";s:23:\"force_from_name_replace\";b:0;s:13:\"smtp_settings\";a:9:{s:4:\"host\";s:14:\"smtp.gmail.com\";s:15:\"type_encryption\";s:3:\"ssl\";s:4:\"port\";s:3:\"465\";s:13:\"autentication\";s:3:\"yes\";s:8:\"username\";s:32:\"automatizacionmicuenta@gmail.com\";s:8:\"password\";s:12:\"VGlnbzIwMTk=\";s:12:\"enable_debug\";b:0;s:12:\"insecure_ssl\";b:0;s:12:\"encrypt_pass\";b:0;}s:15:\"allowed_domains\";s:20:\"c2VydmlhdXRvcy50aw==\";s:14:\"reply_to_email\";s:0:\"\";s:17:\"email_ignore_list\";s:0:\"\";s:19:\"enable_domain_check\";b:0;}','yes'),(1764,'smtp_test_mail','a:3:{s:10:\"swpsmtp_to\";s:19:\"pruebas@yopmail.com\";s:15:\"swpsmtp_subject\";s:8:\"Probando\";s:15:\"swpsmtp_message\";s:8:\"Probando\";}','yes'),(1769,'_site_transient_update_plugins','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1564478510;s:7:\"checked\";a:6:{s:19:\"akismet/akismet.php\";s:5:\"4.1.2\";s:29:\"easy-wp-smtp/easy-wp-smtp.php\";s:7:\"1.3.9.1\";s:27:\"simple-gomaps-10/gomaps.php\";s:3:\"1.0\";s:21:\"hello-dolly/hello.php\";s:5:\"1.7.2\";s:27:\"woowhatsapp/woowhatsapp.php\";s:5:\"1.3.5\";s:33:\"yandex-metrica/yandex-metrica.php\";s:5:\"1.8.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:5:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"easy-wp-smtp/easy-wp-smtp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/easy-wp-smtp\";s:4:\"slug\";s:12:\"easy-wp-smtp\";s:6:\"plugin\";s:29:\"easy-wp-smtp/easy-wp-smtp.php\";s:11:\"new_version\";s:7:\"1.3.9.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/easy-wp-smtp/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/plugin/easy-wp-smtp.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/easy-wp-smtp/assets/icon-128x128.png?rev=1242044\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/easy-wp-smtp/assets/banner-772x250.png?rev=1650323\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"hello-dolly/hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:21:\"hello-dolly/hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"woowhatsapp/woowhatsapp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woowhatsapp\";s:4:\"slug\";s:11:\"woowhatsapp\";s:6:\"plugin\";s:27:\"woowhatsapp/woowhatsapp.php\";s:11:\"new_version\";s:5:\"1.3.5\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woowhatsapp/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woowhatsapp.1.3.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woowhatsapp/assets/icon-256x256.png?rev=1942033\";s:2:\"1x\";s:64:\"https://ps.w.org/woowhatsapp/assets/icon-256x256.png?rev=1942033\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:33:\"yandex-metrica/yandex-metrica.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/yandex-metrica\";s:4:\"slug\";s:14:\"yandex-metrica\";s:6:\"plugin\";s:33:\"yandex-metrica/yandex-metrica.php\";s:11:\"new_version\";s:5:\"1.8.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/yandex-metrica/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/yandex-metrica.1.8.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/yandex-metrica/assets/icon-256x256.jpg?rev=1416613\";s:2:\"1x\";s:67:\"https://ps.w.org/yandex-metrica/assets/icon-128x128.jpg?rev=1416613\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/yandex-metrica/assets/banner-772x250.png?rev=659834\";}s:11:\"banners_rtl\";a:0:{}}}}','no'),(1771,'metrica_options','a:16:{s:10:\"counter_id\";i:53929258;s:8:\"webvisor\";b:1;s:8:\"clickmap\";b:1;s:10:\"tracklinks\";b:1;s:14:\"accurate_track\";b:1;s:10:\"track_hash\";b:1;s:15:\"track-logged-in\";b:1;s:13:\"untrack-roles\";a:5:{i:0;s:13:\"administrator\";i:1;s:6:\"editor\";i:2;s:6:\"author\";i:3;s:11:\"contributor\";i:4;s:10:\"subscriber\";}s:19:\"widget-access-roles\";a:5:{i:0;s:13:\"administrator\";i:1;s:6:\"editor\";i:2;s:6:\"author\";i:3;s:11:\"contributor\";i:4;s:10:\"subscriber\";}s:8:\"backward\";b:0;s:15:\"new_yandex_code\";b:1;s:18:\"dispatch_ecommerce\";b:0;s:24:\"ecommerce_container_name\";s:9:\"dataLayer\";s:15:\"tracker-address\";s:0:\"\";s:8:\"authcode\";i:5004619;s:12:\"access_token\";s:39:\"AgAAAAA00ymOAAH1qCyrkQBUkUh8tYq_UGz2g48\";}','yes'),(1772,'widget_metrica_widget','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(1789,'_site_transient_update_themes','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1564478510;s:7:\"checked\";a:1:{s:15:\"twentyseventeen\";s:3:\"2.2\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}','no'),(1794,'_transient_is_multi_author','0','yes'),(2555,'_site_transient_timeout_theme_roots','1564482632','no'),(2556,'_site_transient_theme_roots','a:1:{s:15:\"twentyseventeen\";s:7:\"/themes\";}','no'),(2557,'_transient_doing_cron','1564517596.8466920852661132812500','yes'),(2558,'_transient_timeout_easy_wp_smtp_sd_code','1564560808','no'),(2559,'_transient_easy_wp_smtp_sd_code','f92edfdfb18cc87d24f8214920045b95','no');
/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_postmeta`
--

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;
INSERT INTO `wp_postmeta` VALUES (1,2,'_wp_page_template','default'),(2,3,'_wp_page_template','default'),(4,6,'_wp_attached_file','2018/12/Serviautos-Fernandez.png'),(5,6,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:952;s:6:\"height\";i:960;s:4:\"file\";s:32:\"2018/12/Serviautos-Fernandez.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"Serviautos-Fernandez-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"Serviautos-Fernandez-298x300.png\";s:5:\"width\";i:298;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"Serviautos-Fernandez-768x774.png\";s:5:\"width\";i:768;s:6:\"height\";i:774;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"theta-thumb\";a:4:{s:4:\"file\";s:32:\"Serviautos-Fernandez-184x186.png\";s:5:\"width\";i:184;s:6:\"height\";i:186;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(6,7,'_wp_attached_file','2018/12/cropped-Serviautos-Fernandez.png'),(7,7,'_wp_attachment_context','custom-logo'),(8,7,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:40:\"2018/12/cropped-Serviautos-Fernandez.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(11,8,'_wp_attached_file','2018/12/TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03.jpg'),(12,8,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:675;s:4:\"file\";s:64:\"2018/12/TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:64:\"TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:64:\"TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:64:\"TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:65:\"TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:64:\"TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"4.5\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:12:\"Canon EOS 6D\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1450805408\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:2:\"27\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:6:\"0.0125\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),(13,9,'_wp_attached_file','2018/12/cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03.jpg'),(14,9,'_wp_attachment_context','custom-header'),(15,9,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:4:\"file\";s:72:\"2018/12/cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:72:\"cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:72:\"cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-300x180.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:72:\"cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-768x461.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:461;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:73:\"cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-1024x614.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:614;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:72:\"cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:17:\"attachment_parent\";i:8;}'),(16,9,'_wp_attachment_custom_header_last_used_twentyseventeen','1544621402'),(17,9,'_wp_attachment_is_custom_header','twentyseventeen'),(21,1,'_edit_lock','1559666928:1'),(22,1,'_edit_last','1'),(29,1,'_oembed_66e32a4f0880273b11f5161326b77e3a','{{unknown}}'),(45,1,'enclosure','https://www.dropbox.com/s/xitfjjppnsrkryk/Restoration.mp4\r\n0\r\nvideo/mp4\r\n'),(60,26,'_wp_attached_file','2018/12/cropped-Serviautos-Fernandez-1.png'),(61,26,'_wp_attachment_context','custom-logo'),(62,26,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:250;s:6:\"height\";i:250;s:4:\"file\";s:42:\"2018/12/cropped-Serviautos-Fernandez-1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:42:\"cropped-Serviautos-Fernandez-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:42:\"cropped-Serviautos-Fernandez-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(81,1,'_wp_old_slug','hello-world__trashed'),(84,2,'_edit_lock','1545929668:1'),(85,3,'_edit_lock','1545928914:1'),(86,28,'_wp_attached_file','2019/06/WhatsApp-Image-2019-05-26-at-9.12.53-PM-1.jpeg'),(87,28,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:774;s:6:\"height\";i:1032;s:4:\"file\";s:54:\"2019/06/WhatsApp-Image-2019-05-26-at-9.12.53-PM-1.jpeg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:54:\"WhatsApp-Image-2019-05-26-at-9.12.53-PM-1-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:54:\"WhatsApp-Image-2019-05-26-at-9.12.53-PM-1-225x300.jpeg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:55:\"WhatsApp-Image-2019-05-26-at-9.12.53-PM-1-768x1024.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:55:\"WhatsApp-Image-2019-05-26-at-9.12.53-PM-1-768x1024.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:54:\"WhatsApp-Image-2019-05-26-at-9.12.53-PM-1-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(88,29,'_wp_attached_file','2019/06/WhatsApp-Image-2019-05-26-at-9.12.53-PM.jpeg'),(89,29,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1032;s:6:\"height\";i:774;s:4:\"file\";s:52:\"2019/06/WhatsApp-Image-2019-05-26-at-9.12.53-PM.jpeg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:52:\"WhatsApp-Image-2019-05-26-at-9.12.53-PM-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:52:\"WhatsApp-Image-2019-05-26-at-9.12.53-PM-300x225.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:52:\"WhatsApp-Image-2019-05-26-at-9.12.53-PM-768x576.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:53:\"WhatsApp-Image-2019-05-26-at-9.12.53-PM-1024x768.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:52:\"WhatsApp-Image-2019-05-26-at-9.12.53-PM-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(93,28,'_edit_lock','1559667387:1'),(94,28,'_edit_last','1');
/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2018-11-30 21:55:12','2018-11-30 21:55:12','Este trabajo se realiza a un Toyota, el cual se tuvo una baja inversión..\r\n<img class=\"alignnone size-thumbnail wp-image-8\" src=\"http://18.191.134.65/wordpress/wp-content/uploads/2018/12/TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-150x150.jpg\" alt=\"\" width=\"150\" height=\"150\" />\r\n\r\n<video class=\"alignnone size-thumbnail wp-image-8\" src=\"https://www.dropbox.com/s/xitfjjppnsrkryk/Restoration.mp4\" alt=\"\" width=\"150\" height=\"150\" />\r\n','Restauración FJ','','publish','open','open','','hello-world','','','2018-12-27 11:38:49','2018-12-27 16:38:49','',0,'http://18.191.134.65/wordpress/?p=1',0,'post','',0),(2,1,'2018-11-30 21:55:12','2018-11-30 21:55:12','This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://18.191.134.65/wordpress/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!','Sample Page','','publish','closed','open','','sample-page','','','2018-11-30 21:55:12','2018-11-30 21:55:12','',0,'http://18.191.134.65/wordpress/?page_id=2',0,'page','',0),(3,1,'2018-11-30 21:55:12','2018-11-30 21:55:12','<h2>Who we are</h2><p>Our website address is: http://18.191.134.65/wordpress.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>','Privacy Policy','','draft','closed','open','','privacy-policy','','','2018-11-30 21:55:12','2018-11-30 21:55:12','',0,'http://18.191.134.65/wordpress/?page_id=3',0,'page','',0),(6,1,'2018-12-04 13:47:39','2018-12-04 13:47:39','','Serviautos Fernandez','Su técnico amigo','inherit','open','closed','','serviautos-fernandez','','','2018-12-04 14:35:02','2018-12-04 14:35:02','',1,'http://18.191.134.65/wordpress/wp-content/uploads/2018/12/Serviautos-Fernandez.png',0,'attachment','image/png',0),(7,1,'2018-12-04 13:51:58','2018-12-04 13:51:58','http://18.191.134.65/wordpress/wp-content/uploads/2018/12/cropped-Serviautos-Fernandez.png','cropped-Serviautos-Fernandez.png','','inherit','open','closed','','cropped-serviautos-fernandez-png','','','2018-12-04 13:51:58','2018-12-04 13:51:58','',0,'http://18.191.134.65/wordpress/wp-content/uploads/2018/12/cropped-Serviautos-Fernandez.png',0,'attachment','image/png',0),(8,1,'2018-12-04 14:24:42','2018-12-04 14:24:42','','ToyotaFJ','','inherit','open','closed','','thefjcompany_1978_fj40_skyblue_scottsdale_outdoor_03','','','2018-12-04 16:03:34','2018-12-04 16:03:34','',1,'http://18.191.134.65/wordpress/wp-content/uploads/2018/12/TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03.jpg',0,'attachment','image/jpeg',0),(9,1,'2018-12-04 14:25:57','2018-12-04 14:25:57','','cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03.jpg','','inherit','open','closed','','cropped-thefjcompany_1978_fj40_skyblue_scottsdale_outdoor_03-jpg','','','2018-12-04 14:25:57','2018-12-04 14:25:57','',0,'http://18.191.134.65/wordpress/wp-content/uploads/2018/12/cropped-TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03.jpg',0,'attachment','image/jpeg',0),(12,1,'2018-12-04 14:34:54','2018-12-04 14:34:54','Somos una empresa con muchos años en el mercado, realizamos reparaciones, restauraciones, mantenimiento. No somos el prototipo  de mecánicos, somos sus amigos.','Su taller amigo','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 14:34:54','2018-12-04 14:34:54','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(13,1,'2018-12-04 14:35:09','2018-12-04 14:35:09','[caption id=\"attachment_6\" align=\"alignnone\" width=\"298\"]<img class=\"size-medium wp-image-6\" src=\"http://18.191.134.65/wordpress/wp-content/uploads/2018/12/Serviautos-Fernandez-298x300.png\" alt=\"\" width=\"298\" height=\"300\" /> Su técnico amigo[/caption]\r\n\r\nSomos una empresa con muchos años en el mercado, realizamos reparaciones, restauraciones, mantenimiento. No somos el prototipo  de mecánicos, somos sus amigos.','Su taller amigo','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 14:35:09','2018-12-04 14:35:09','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(15,1,'2018-12-04 14:38:40','2018-12-04 14:38:40','[caption id=\"attachment_6\" align=\"alignnone\" width=\"298\"]<img class=\"size-medium wp-image-6\" src=\"http://18.191.134.65/wordpress/wp-content/uploads/2018/12/Serviautos-Fernandez-298x300.png\" alt=\"\" width=\"298\" height=\"300\" /> Su técnico amigo[/caption]\r\n\r\nSomos una empresa con muchos años en el mercado, realizamos reparaciones, restauraciones, mantenimiento. No somos el prototipo  de mecánicos, somos sus amigos.','Su tecnico amigo','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 14:38:40','2018-12-04 14:38:40','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(16,1,'2018-12-04 15:29:41','2018-12-04 15:29:41','Este trabajo se realiza a un Toyota, el cual se tuvo una baja inversión.\r\n\r\n<a href=\"https://www.dropbox.com/s/xitfjjppnsrkryk/Restoration.mp4?dl=0\">Restauración FJ</a>','Restauración FJ','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 15:29:41','2018-12-04 15:29:41','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(17,1,'2018-12-04 15:31:03','2018-12-04 15:31:03','Este trabajo se realiza a un Toyota, el cual se tuvo una baja inversión.\r\n\r\n<video class=\"vjs-tech\" id=\"vjs_video_3_html5_api\" tabindex=\"-1\" preload=\"auto\" src=\"blob:https://www.dropbox.com/6c084d4c-a560-406e-8db9-d5170491634e\"><source src=\"/playlist/Fotos%20edit/New%20Folder/Restoration.mp4?_subject_uid=75239954\" type=\"application/vnd.apple.mpegurl\"></video>','Restauración FJ','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 15:31:03','2018-12-04 15:31:03','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(18,1,'2018-12-04 16:01:13','2018-12-04 16:01:13','Este trabajo se realiza a un Toyota, el cual se tuvo una baja inversión.\r\n\r\n<video class=\"vjs-tech\" id=\"vjs_video_1_html5_api\" tabindex=\"-1\" preload=\"auto\" src=\"blob:https://www.dropbox.com/4d015f2b-6ec1-43e1-b617-33ebc23cbc4d\"></video>','Restauración FJ','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 16:01:13','2018-12-04 16:01:13','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(19,1,'2018-12-04 16:01:32','2018-12-04 16:01:32','Este trabajo se realiza a un Toyota, el cual se tuvo una baja inversión.\r\n\r\n<video src=\"blob:https://www.dropbox.com/4d015f2b-6ec1-43e1-b617-33ebc23cbc4d\"></video>','Restauración FJ','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 16:01:32','2018-12-04 16:01:32','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(20,1,'2018-12-04 16:01:53','2018-12-04 16:01:53','Este trabajo se realiza a un Toyota, el cual se tuvo una baja inversión.\r\n\r\n<video src=\"https://www.dropbox.com/4d015f2b-6ec1-43e1-b617-33ebc23cbc4d\"></video>','Restauración FJ','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 16:01:53','2018-12-04 16:01:53','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(21,1,'2018-12-04 16:02:31','2018-12-04 16:02:31','Este trabajo se realiza a un Toyota, el cual se tuvo una baja inversión.\r\n\r\n<video src=\"https://www.dropbox.com/s/xitfjjppnsrkryk/Restoration.mp4\"></video>','Restauración FJ','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 16:02:31','2018-12-04 16:02:31','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(22,1,'2018-12-04 16:02:40','2018-12-04 16:02:40','Este trabajo se realiza a un Toyota, el cual se tuvo una baja inversión..\r\n\r\n<video src=\"https://www.dropbox.com/s/xitfjjppnsrkryk/Restoration.mp4\"></video>','Restauración FJ','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 16:02:40','2018-12-04 16:02:40','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(23,1,'2018-12-04 16:06:24','2018-12-04 16:06:24','Este trabajo se realiza a un Toyota, el cual se tuvo una baja inversión..\r\n<img class=\"alignnone size-thumbnail wp-image-8\" src=\"http://18.191.134.65/wordpress/wp-content/uploads/2018/12/TheFJCompany_1978_FJ40_SkyBlue_Scottsdale_Outdoor_03-150x150.jpg\" alt=\"\" width=\"150\" height=\"150\" />\r\n\r\n<video class=\"alignnone size-thumbnail wp-image-8\" src=\"https://www.dropbox.com/s/xitfjjppnsrkryk/Restoration.mp4\" alt=\"\" width=\"150\" height=\"150\" />\r\n','Restauración FJ','','inherit','closed','closed','','1-revision-v1','','','2018-12-04 16:06:24','2018-12-04 16:06:24','',1,'http://18.191.134.65/wordpress/index.php/2018/12/04/1-revision-v1/',0,'revision','',0),(26,1,'2018-12-12 13:22:51','2018-12-12 13:22:51','http://18.191.134.65/wordpress/wp-content/uploads/2018/12/cropped-Serviautos-Fernandez-1.png','cropped-Serviautos-Fernandez-1.png','','inherit','open','closed','','cropped-serviautos-fernandez-1-png','','','2018-12-12 13:22:51','2018-12-12 13:22:51','',0,'http://18.191.134.65/wordpress/wp-content/uploads/2018/12/cropped-Serviautos-Fernandez-1.png',0,'attachment','image/png',0),(28,1,'2019-06-04 11:47:59','2019-06-04 16:47:59','','Toyota restauracion','','inherit','open','closed','','whatsapp-image-2019-05-26-at-9-12-53-pm-1','','','2019-06-04 11:58:47','2019-06-04 16:58:47','',1,'http://serviautos.tk/wp-content/uploads/2019/06/WhatsApp-Image-2019-05-26-at-9.12.53-PM-1.jpeg',0,'attachment','image/jpeg',0),(29,1,'2019-06-04 11:48:00','2019-06-04 16:48:00','','Su técnico amigo verificando','','inherit','open','closed','','whatsapp-image-2019-05-26-at-9-12-53-pm','','','2019-06-04 11:59:24','2019-06-04 16:59:24','',0,'http://serviautos.tk/wp-content/uploads/2019/06/WhatsApp-Image-2019-05-26-at-9.12.53-PM.jpeg',0,'attachment','image/jpeg',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_relationships`
--

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;
INSERT INTO `wp_term_relationships` VALUES (1,1,0),(1,2,0);
/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_taxonomy`
--

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` VALUES (1,1,'category','',0,1),(2,2,'post_format','',0,1);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_termmeta`
--

LOCK TABLES `wp_termmeta` WRITE;
/*!40000 ALTER TABLE `wp_termmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_termmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'Uncategorized','uncategorized',0),(2,'post-format-image','post-format-image',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_usermeta`
--

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'nickname','handresc1127'),(2,1,'first_name',''),(3,1,'last_name',''),(4,1,'description',''),(5,1,'rich_editing','true'),(6,1,'syntax_highlighting','true'),(7,1,'comment_shortcuts','false'),(8,1,'admin_color','fresh'),(9,1,'use_ssl','0'),(10,1,'show_admin_bar_front','true'),(11,1,'locale',''),(12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(13,1,'wp_user_level','10'),(14,1,'dismissed_wp_pointers','wp496_privacy,text_widget_custom_html,theme_editor_notice'),(15,1,'show_welcome_panel','1'),(16,1,'session_tokens','a:1:{s:64:\"c8d91c1c7c6761881381655c3ff3774cf7cb87deaf7a5a7e05e984c72906d846\";a:4:{s:10:\"expiration\";i:1559838273;s:2:\"ip\";s:14:\"200.13.250.200\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";s:5:\"login\";i:1559665473;}}'),(17,1,'wp_dashboard_quick_press_last_post_id','27'),(18,1,'community-events-location','a:1:{s:2:\"ip\";s:12:\"200.13.250.0\";}'),(19,1,'wp_user-settings','libraryContent=browse&editor=html&post_dfw=off&imgsize=thumbnail&widgets_access=off&mfold=o'),(20,1,'wp_user-settings-time','1559667139'),(21,1,'managenav-menuscolumnshidden','a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),(22,1,'metaboxhidden_nav-menus','a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),(23,2,'nickname','rainahavelock81'),(24,2,'first_name',''),(25,2,'last_name',''),(26,2,'description',''),(27,2,'rich_editing','true'),(28,2,'syntax_highlighting','true'),(29,2,'comment_shortcuts','false'),(30,2,'admin_color','fresh'),(31,2,'use_ssl','0'),(32,2,'show_admin_bar_front','true'),(33,2,'locale',''),(34,2,'wp_capabilities','a:1:{s:10:\"subscriber\";b:1;}'),(35,2,'wp_user_level','0'),(36,2,'default_password_nag','1'),(37,3,'nickname','henriettaluttrel'),(38,3,'first_name',''),(39,3,'last_name',''),(40,3,'description',''),(41,3,'rich_editing','true'),(42,3,'syntax_highlighting','true'),(43,3,'comment_shortcuts','false'),(44,3,'admin_color','fresh'),(45,3,'use_ssl','0'),(46,3,'show_admin_bar_front','true'),(47,3,'locale',''),(48,3,'wp_capabilities','a:1:{s:10:\"subscriber\";b:1;}'),(49,3,'wp_user_level','0'),(50,3,'default_password_nag','1'),(51,4,'nickname','marcy611125'),(52,4,'first_name',''),(53,4,'last_name',''),(54,4,'description',''),(55,4,'rich_editing','true'),(56,4,'syntax_highlighting','true'),(57,4,'comment_shortcuts','false'),(58,4,'admin_color','fresh'),(59,4,'use_ssl','0'),(60,4,'show_admin_bar_front','true'),(61,4,'locale',''),(62,4,'wp_capabilities','a:1:{s:10:\"subscriber\";b:1;}'),(63,4,'wp_user_level','0'),(64,4,'default_password_nag','1'),(65,5,'nickname','marlonsherrill'),(66,5,'first_name',''),(67,5,'last_name',''),(68,5,'description',''),(69,5,'rich_editing','true'),(70,5,'syntax_highlighting','true'),(71,5,'comment_shortcuts','false'),(72,5,'admin_color','fresh'),(73,5,'use_ssl','0'),(74,5,'show_admin_bar_front','true'),(75,5,'locale',''),(76,5,'wp_capabilities','a:1:{s:10:\"subscriber\";b:1;}'),(77,5,'wp_user_level','0'),(78,5,'default_password_nag','1'),(79,6,'nickname','cameronworsnop4'),(80,6,'first_name',''),(81,6,'last_name',''),(82,6,'description',''),(83,6,'rich_editing','true'),(84,6,'syntax_highlighting','true'),(85,6,'comment_shortcuts','false'),(86,6,'admin_color','fresh'),(87,6,'use_ssl','0'),(88,6,'show_admin_bar_front','true'),(89,6,'locale',''),(90,6,'wp_capabilities','a:1:{s:10:\"subscriber\";b:1;}'),(91,6,'wp_user_level','0'),(92,6,'default_password_nag','1');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_users`
--

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
INSERT INTO `wp_users` VALUES (1,'handresc1127','$P$BShpbRCWSEnlJdDa4L4jX6XQ/DZDvg/','handresc1127','h.andresc1127@gmail.com','','2018-11-30 21:55:12','1559656621:$P$B9QBPtYNOPK4mbqs/MNgaIIGLO8riG1',0,'handresc1127'),(2,'rainahavelock81','$P$B0AXmapwVA4CkgtS4iCXlXeKCY5Pto/','rainahavelock81','Raina.Havelock95@kristel1993.cottonsleepingbags.com','','2018-12-28 13:29:38','1546003779:$P$BFbf4m2PgFPeErObV03cX8j1VUuaxh/',0,'rainahavelock81'),(3,'henriettaluttrel','$P$BgyEU/DDdEnOQahszf7lX1gH/j5tN20','henriettaluttrel','alizabates@discard.email','','2018-12-28 21:36:53','1546033013:$P$B9su40smPBdG2sLE/ljKNGSrx.Zkbg0',0,'henriettaluttrel'),(4,'marcy611125','$P$BAY8gbO5U3YjbJmoKoZs1UOenUGsc2.','marcy611125','Marcy.Robins84@turton81.nicewoodenbaskets.com','','2018-12-29 00:53:58','1546044838:$P$B3wwhKg5SkaIcUqG3hJgG06c9M09W2.',0,'marcy611125'),(5,'marlonsherrill','$P$BbEvkdPOpIDiCI6ojyi4z1x1WwpkD40','marlonsherrill','Marlon-Sherrill68@dagel4.plasticvouchercards.com','','2018-12-29 00:56:14','1546044974:$P$Bboc.RX3wSiGAyqsZhnN4wZGEZixeK/',0,'marlonsherrill'),(6,'cameronworsnop4','$P$BKaiZGS3cW41dZ.uwG3OUr1bGNhoNg.','cameronworsnop4','Cameron-Worsnop7@harrer.fastlasermouses.com','','2018-12-29 09:55:20','1546077320:$P$Bie5YH02gkOYhdjC35y0KetIN5brn70',0,'cameronworsnop4');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-30 20:27:28
